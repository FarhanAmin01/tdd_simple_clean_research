import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ADD_CAT_REPOSITORY } from '../application/protocols/add-cat-repository';
import { DELETE_CAT_REPOSITORY } from '../application/protocols/delete-cat-repository';
import { FIND_CAT_REPOSITORY } from '../application/protocols/find-cat-repository';
import { UPDATE_CAT_REPOSITORY } from '../application/protocols/update-cat.repository';

import { Cat } from './typeorn/entities/cat.entity';
import { CatsRepository } from './typeorn/repositories/cat-repository';

@Module({
  imports: [TypeOrmModule.forFeature([Cat])],
  exports: [
    ADD_CAT_REPOSITORY,
    FIND_CAT_REPOSITORY,
    DELETE_CAT_REPOSITORY,
    UPDATE_CAT_REPOSITORY,
  ],
  providers: [
    { provide: ADD_CAT_REPOSITORY, useClass: CatsRepository },
    { provide: FIND_CAT_REPOSITORY, useClass: CatsRepository },
    { provide: UPDATE_CAT_REPOSITORY, useClass: CatsRepository },
    { provide: DELETE_CAT_REPOSITORY, useClass: CatsRepository },
  ],
})
export class InfraModule {}
