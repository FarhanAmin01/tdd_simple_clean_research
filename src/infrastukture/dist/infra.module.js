"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.InfraModule = void 0;
var common_1 = require("@nestjs/common");
var typeorm_1 = require("@nestjs/typeorm");
var add_cat_repository_1 = require("../application/protocols/add-cat-repository");
var delete_cat_repository_1 = require("../application/protocols/delete-cat-repository");
var find_cat_repository_1 = require("../application/protocols/find-cat-repository");
var update_cat_repository_1 = require("../application/protocols/update-cat.repository");
var cat_entity_1 = require("./typeorn/entities/cat.entity");
var cat_repository_1 = require("./typeorn/repositories/cat-repository");
var InfraModule = /** @class */ (function () {
    function InfraModule() {
    }
    InfraModule = __decorate([
        common_1.Module({
            imports: [typeorm_1.TypeOrmModule.forFeature([cat_entity_1.Cat])],
            exports: [
                add_cat_repository_1.ADD_CAT_REPOSITORY,
                find_cat_repository_1.FIND_CAT_REPOSITORY,
                delete_cat_repository_1.DELETE_CAT_REPOSITORY,
                update_cat_repository_1.UPDATE_CAT_REPOSITORY,
            ],
            providers: [
                { provide: add_cat_repository_1.ADD_CAT_REPOSITORY, useClass: cat_repository_1.CatsRepository },
                { provide: find_cat_repository_1.FIND_CAT_REPOSITORY, useClass: cat_repository_1.CatsRepository },
                { provide: update_cat_repository_1.UPDATE_CAT_REPOSITORY, useClass: cat_repository_1.CatsRepository },
                { provide: delete_cat_repository_1.DELETE_CAT_REPOSITORY, useClass: cat_repository_1.CatsRepository },
            ]
        })
    ], InfraModule);
    return InfraModule;
}());
exports.InfraModule = InfraModule;
