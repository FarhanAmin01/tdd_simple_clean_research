/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AddCatRepository } from '../../../application/protocols/add-cat-repository';
import { FindCatRepository } from '../../../application/protocols/find-cat-repository';
import { DeleteCatRepository } from '../../../application/protocols/delete-cat-repository';
import { Cat } from '../entities/cat.entity';
import { UpdateCatRepository } from '../../../application/protocols/update-cat.repository';

@Injectable()
export class CatsRepository
  implements AddCatRepository, FindCatRepository, DeleteCatRepository
{
  constructor(
    @InjectRepository(Cat)
    private catsRepository: Repository<Cat>,
  ) {}

  async add(
    catData: AddCatRepository.Params,
  ): Promise<AddCatRepository.Result> {
    const cat = await this.catsRepository.save(
      this.catsRepository.create(catData),
    );

    return cat;
  }

  async find(
    data: FindCatRepository.Params,
  ): Promise<FindCatRepository.Result> {
    const { id } = data;
    const cat = await this.catsRepository.findOne({ id });

    return cat;
  }

  async delete(
    delCat: DeleteCatRepository.Params,
  ): Promise<DeleteCatRepository.Result> {
    const { id } = delCat;
    await this.catsRepository.delete(id);
    return;
  }

  async update(
    updateCat: UpdateCatRepository.Params,
  ): Promise<UpdateCatRepository.Result> {
    const {  name, breed , age } = updateCat
    await this.catsRepository.update( name , breed, age)
    return;
  }
}
