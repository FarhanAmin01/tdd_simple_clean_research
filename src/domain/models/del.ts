/* eslint-disable prettier/prettier */
export type CatModelDelete = {
  id: number;
  name: string;
  age: number;
  breed: string;
};
