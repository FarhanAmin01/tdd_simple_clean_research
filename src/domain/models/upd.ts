/* eslint-disable prettier/prettier */
export type UpdateCatModel = {
  id: number;
  name?: string;
  age?: number;
  breed?: string;
};
