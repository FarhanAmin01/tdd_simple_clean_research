/* eslint-disable prettier/prettier */
import { CatModel } from '../models/add';

export const DELETE_CAT = 'DELETE_CAT';

export interface DeleteCat {
  execute: (cat: DeleteCat.Params) => Promise<DeleteCat.Result>;
}

export namespace DeleteCat {
  export type Params = {
    id : string;
  };

  export type Result = CatModel;
}
