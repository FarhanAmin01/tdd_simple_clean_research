/* eslint-disable prettier/prettier */
import { UpdateCatModel } from '../models/upd';

export const UPDATE_CAT = 'UPDATE_CAT';

export interface UpdateCat {
  execute: (cat: UpdateCat.Params) => Promise<UpdateCat.Result>;
}

export namespace UpdateCat {
  export type Params = {
    id? : string;
    name?: string;
    age?: number;
    breed?: string;
  };
  export type Result = UpdateCatModel;
}
