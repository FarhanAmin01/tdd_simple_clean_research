import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Inject,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';
import { AddCat, ADD_CAT } from '../../domain/usecases/add-cat';
import { DeleteCat, DELETE_CAT } from '../../domain/usecases/delete-cat';
import { FindCat, FIND_CAT } from '../../domain/usecases/find-cat';
import { UpdateCat, UPDATE_CAT } from '../../domain/usecases/update-cat';
import { AddCatDTO, DeleteCatDTO } from './add-cat.dto';

@Controller('cat')
export class CatController {
  constructor(
    @Inject(ADD_CAT)
    private readonly addCat: AddCat,
    @Inject(FIND_CAT)
    private readonly findCat: FindCat,
    @Inject(DELETE_CAT)
    private readonly deleteCat: DeleteCat,
    @Inject(UPDATE_CAT)
    private readonly updateCat: UpdateCat,
  ) {}

  @Get(':id')
  async getCat(@Param('id', ParseIntPipe) id: number) {
    const cat = await this.findCat.execute({ id });

    if (!cat) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    return cat;
  }

  @Post()
  async createCat(@Body() body: AddCatDTO) {
    return await this.addCat.execute(body);
  }

  @Put()
  async update(@Body() body: DeleteCatDTO) {
    return await this.updateCat.execute(body);
  }

  @Delete('/:id')
  async delete(@Param('id', ParseIntPipe) id: string) {
    return this.deleteCat.execute({ id });
  }
}
