import { Module } from '@nestjs/common';
import { DataModule } from '../application/data.module';
import { CatController } from './cat/cat-controller';

@Module({
  imports: [DataModule],
  controllers: [CatController],
})
export class PresentationModule {}
