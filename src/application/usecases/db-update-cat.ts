/* eslint-disable prettier/prettier */
import { Inject, Injectable } from '@nestjs/common';
import { UpdateCat } from 'src/domain/usecases/update-cat';
import {
  UpdateCatRepository,
  UPDATE_CAT_REPOSITORY,
} from '../protocols/update-cat.repository';

@Injectable()
export class DbUpdateCat implements UpdateCat {
  constructor(
    @Inject(UPDATE_CAT_REPOSITORY)
    private readonly updateCatRepository: UpdateCatRepository,
  ) {}

  execute(catData: UpdateCat.Params): Promise<UpdateCat.Result> {
    return this.updateCatRepository.update(catData);
  }
}
