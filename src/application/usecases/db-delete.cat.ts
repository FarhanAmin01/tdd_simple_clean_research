/* eslint-disable prettier/prettier */
import { Inject, Injectable } from '@nestjs/common';
import { DeleteCat } from '../../domain/usecases/delete-cat';
import {
  DeleteCatRepository,
  DELETE_CAT_REPOSITORY,
} from '../protocols/delete-cat-repository';
@Injectable()
export class DbDeleteCat implements DeleteCat {
  constructor(
    @Inject(DELETE_CAT_REPOSITORY)
    private readonly deleteCatRepository: DeleteCatRepository,
  ) {}

  async execute(catData: DeleteCat.Params): Promise<DeleteCat.Result> {
    const cat = await this.deleteCatRepository.delete(catData);
    return cat
  }
}
