import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cat } from '../infrastukture/typeorn/entities/cat.entity';
import { PresentationModule } from '../presentation/presentation.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3307,
      username: 'root',
      password: 'farhan',
      database: 'learnttd',
      entities: [Cat],
      synchronize: true,
      logging: true,
    }),
    PresentationModule,
  ],
})
export class AppModule {}
