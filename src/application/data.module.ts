import { Module } from '@nestjs/common';
import { ADD_CAT } from 'src/domain/usecases/add-cat';
import { FIND_CAT } from 'src/domain/usecases/find-cat';
import { DELETE_CAT } from '../domain/usecases/delete-cat';
import { UPDATE_CAT } from '../domain/usecases/update-cat';
import { InfraModule } from '../infrastukture/infra.module';
import { DbAddCat } from './usecases/db-add-cat';
import { DbDeleteCat } from './usecases/db-delete.cat';
import { DbFindCat } from './usecases/db-find-cat';
import { DbUpdateCat } from './usecases/db-update-cat';

@Module({
  exports: [ADD_CAT, FIND_CAT, DELETE_CAT, UPDATE_CAT],
  imports: [InfraModule],
  providers: [
    { provide: ADD_CAT, useClass: DbAddCat },
    { provide: FIND_CAT, useClass: DbFindCat },
    { provide: DELETE_CAT, useClass: DbDeleteCat },
    { provide: UPDATE_CAT, useClass: DbUpdateCat },
  ],
})
export class DataModule {}
