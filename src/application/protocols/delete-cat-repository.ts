/* eslint-disable prettier/prettier */
import { DeleteCat } from 'src/domain/usecases/delete-cat';
import { CatModelDelete } from 'src/domain/models/del';

export const DELETE_CAT_REPOSITORY = 'DELETE_CAT_REPOSITORY';

export interface DeleteCatRepository {
  delete: (catData: DeleteCatRepository.Params) => Promise<DeleteCatRepository.Result>;
}

export namespace DeleteCatRepository {
  export type Params = DeleteCat.Params;
  export type Result = CatModelDelete;
}
