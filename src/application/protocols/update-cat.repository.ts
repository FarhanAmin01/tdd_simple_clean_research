/* eslint-disable prettier/prettier */
import { UpdateCatModel } from 'src/domain/models/upd';
import { UpdateCat } from '../../domain/usecases/update-cat';

export const UPDATE_CAT_REPOSITORY = 'UPDATE_CAT_REPOSITORY';

export interface UpdateCatRepository {
  update: (
    Data: UpdateCatRepository.Params,
  ) => Promise<UpdateCatRepository.Result>;
}

export namespace UpdateCatRepository {
  export type Params = UpdateCat.Params;
  export type Result = UpdateCatModel;
}
