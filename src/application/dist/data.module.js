"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.DataModule = void 0;
var common_1 = require("@nestjs/common");
var add_cat_1 = require("src/domain/usecases/add-cat");
var find_cat_1 = require("src/domain/usecases/find-cat");
var delete_cat_1 = require("../domain/usecases/delete-cat");
var update_cat_1 = require("../domain/usecases/update-cat");
var infra_module_1 = require("../infrastukture/infra.module");
var db_add_cat_1 = require("./usecases/db-add-cat");
var db_delete_cat_1 = require("./usecases/db-delete.cat");
var db_find_cat_1 = require("./usecases/db-find-cat");
var db_update_cat_1 = require("./usecases/db-update-cat");
var DataModule = /** @class */ (function () {
    function DataModule() {
    }
    DataModule = __decorate([
        common_1.Module({
            exports: [add_cat_1.ADD_CAT, find_cat_1.FIND_CAT, delete_cat_1.DELETE_CAT, update_cat_1.UPDATE_CAT],
            imports: [infra_module_1.InfraModule],
            providers: [
                { provide: add_cat_1.ADD_CAT, useClass: db_add_cat_1.DbAddCat },
                { provide: find_cat_1.FIND_CAT, useClass: db_find_cat_1.DbFindCat },
                { provide: delete_cat_1.DELETE_CAT, useClass: db_delete_cat_1.DbDeleteCat },
                { provide: update_cat_1.UPDATE_CAT, useClass: db_update_cat_1.DbUpdateCat },
            ]
        })
    ], DataModule);
    return DataModule;
}());
exports.DataModule = DataModule;
